<?php

/**
 * @file
 * Primarily Drupal hooks and global API functions.
 */

define('OFF_CANVAS_RESPONSIVE_MENU', 'off_canvas_responsive_menu');
define('OFF_CANVAS_RESPONSIVE_MENU_DEPTH', 'off_canvas_responsive_menu_depth');
define('OFF_CANVAS_RESPONSIVE_MENU_NAME', 'off_canvas_responsive_menu_name');

/**
 * Implements hook_block_info().
 */
function off_canvas_responsive_menu_block_info() {
  $blocks = array();
  $blocks[OFF_CANVAS_RESPONSIVE_MENU] = array(
    'info' => t('Off Canvas Responsive Menu'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function off_canvas_responsive_menu_block_view($delta = '') {
  $block = array();
  if ($delta == OFF_CANVAS_RESPONSIVE_MENU) {
    $path = drupal_get_path('module', 'off_canvas_responsive_menu');
    $library_path = libraries_get_path('sassy_off_canvas_nav-master');
    $block['subject'] = '';
    if (empty($library_path)) {
      $block['content'] = array(
        '#markup' => t('Sassy off canvas library not installed. Please install first as per provided steps in Readme.txt'),
      );
    }
    else {
      $block['content'] = array(
        '#markup' => _off_canvas_responsive_menu_block_content(),
        '#attached' => array(
          'css' => array(
            $library_path . '/css/off_canvas_nav.css',
            $library_path . '/css/style.css',
            $path . '/css/off_canvas_nav.css',
          ),
          'js' => array(
            $path . '/js/off_canvas_nav.js',
          ),
        ),
      );
    }
  }
  return $block;
}

/**
 * Implements hook_block_configure().
 */
function off_canvas_responsive_menu_block_configure($delta = '') {
  $form = array();
  if ($delta == OFF_CANVAS_RESPONSIVE_MENU) {
    $menus = menu_load_all();
    $array_menu = array();
    foreach ($menus as $key => $value) {
      $array_menu[$key] = $value['title'];
    }

    $form[OFF_CANVAS_RESPONSIVE_MENU_NAME] = array(
      '#type' => 'select',
      '#title' => t('Menu name'),
      '#options' => $array_menu,
      '#default_value' => variable_get(OFF_CANVAS_RESPONSIVE_MENU_NAME, ''),
      '#description' => t('Selected menu will be use as a menu.'),
    );
    $form[OFF_CANVAS_RESPONSIVE_MENU_DEPTH] = array(
      '#type' => 'select',
      '#title' => t('Menu depth'),
      '#options' => array(
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
      ),
      '#default_value' => variable_get(OFF_CANVAS_RESPONSIVE_MENU_DEPTH, ''),
      '#description' => t('Selected depth will be use as a depth of menu.'),
    );
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function off_canvas_responsive_menu_block_save($delta = '', $edit = array()) {
  if ($delta == OFF_CANVAS_RESPONSIVE_MENU) {
    // Saving variables.
    variable_set(OFF_CANVAS_RESPONSIVE_MENU_NAME, $edit[OFF_CANVAS_RESPONSIVE_MENU_NAME]);
    variable_set(OFF_CANVAS_RESPONSIVE_MENU_DEPTH, $edit[OFF_CANVAS_RESPONSIVE_MENU_DEPTH]);
  }
}

/**
 * Build html of menu.
 *
 * @return string
 *   html output
 */
function _off_canvas_responsive_menu_block_content() {
  $menu_name = variable_get(OFF_CANVAS_RESPONSIVE_MENU_NAME, '');
  $max_depth = variable_get(OFF_CANVAS_RESPONSIVE_MENU_DEPTH, '');
  if (!empty($max_depth)) {
    $menus = menu_tree_all_data($menu_name, NULL, $max_depth);
  }
  else {
    $menus = menu_tree_all_data($menu_name, NULL);
  }
  return theme('off_canvas_responsive_menu_output', array('menus' => $menus));
}

/**
 * Implements hook_theme().
 */
function off_canvas_responsive_menu_theme() {
  return array(
    'off_canvas_responsive_menu_output' => array(
      'variables' => array('menus' => NULL),
      'template' => 'tpl/off-canvas-responsive-menu-output',
    ),
    'off_canvas_responsive_menu_build_html' => array(
      'variables' => array('menus' => NULL),
      'template' => 'tpl/off-canvas-responsive-menu-build-html',
    ),
  );
}

/**
 * Build html of menu output.
 *
 * @param array $menus
 *   A array of menus which will be use for generate a html.
 *
 * @return string
 *   html output
 */
function off_canvas_responsive_menu_build_html($menus = array()) {
  if (!empty($menus)) {
    return theme('off_canvas_responsive_menu_build_html', array('menus' => $menus));
  }
}
