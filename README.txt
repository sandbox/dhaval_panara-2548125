
INTRODUCTION
------------
 This is a sticky multi-level top menu bar that will be converted into a 
 mobile-friendly off-canvas sliding navigation on small screen devices. 
 Useful for responsive websites to provide an user-friendly and efficient 
 navigation system. The navigation will slide out from the edge from the 
 left side of the screen when clicking or touching the toggle button, 
 with a nice CSS3 transition effect. 

 REQUIREMENTS
-------------
- This module requires the following modules:
  - jquery_update (https://drupal.org/project/jquery_update)

FEATURES
--------
The Off canvas responsive menu jQuery plugin has following features:
- HTML 5 based 
- Fully responsive CSS framework generated with SCSS.
- Creates sliding panels as easy as menus.
- Provide up to 10 level hierarchy.
- User friendly
- Sliding navigation on small screen devices
 
INSTALLATION
------------
1 - First, installs the off canvas responsive menu module (standard Drupal way).

2 - Configurations

  2.1 - Sets the version to 1.11 or above at
        /admin/config/development/jquery_update.
  2.2 - Download Sassy Off-Canvas Navigation from url 
        https://github.com/mmonkey/sassy_off_canvas_nav
  2.3 - Extract this zip and copy 'sassy_off_canvas_nav-master' folder to 
        sites/all/libraries

3 - Go to list of blocks
   
   3.1 - Configure 'Off Canvas Responsive Menu' block
   3.2 - Select Menu name
   3.3 - Select Menu depth

Notes
--------------------------------------------------------------------------------
This module is based on HTML5 so this module will work only in HTML 5 based 
theme.

Example
--------------------------------------------------------------------------------
1 - Install latest version of drupal
2 - Install zen theme, enable it and make it default theme
3 - Install off canvas responsive menu module and enable it
4 - Download Sassy Off-Canvas Navigation from url 
    https://github.com/mmonkey/sassy_off_canvas_nav
5 - Extract this zip and copy 'sassy_off_canvas_nav-master' folder to
    sites/all/libraries
6 - Install jquery_update module and enable it
7 - Configure jquery_update module as shown in installation steps
8 - Configure 'Off Canvas Responsive Menu' block
  8.1 - Select Menu name
  8.2 - Select Menu depth
9 - Create hierarchy of menus in selected menu. If menu have submenus then 
    checked "Show as expanded" checkbox of parent menu.
