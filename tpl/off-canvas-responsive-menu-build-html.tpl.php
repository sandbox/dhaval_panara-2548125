<?php
/**
 * @file
 * Build html output for off canvas menu.
 *
 * Available variables:
 * - $menus: array of selected menus.
 */
?>
<ul>
  <?php
  foreach ($menus as $menu) :
    if (empty($menu['link']['hidden'])):
      $menu_title = $menu['link']['link_title'];
      $menu_link = (empty($menu['link']['link_path'])) ? $base_url : $menu['link']['link_path'];
      ?>
      <li>
        <?php
        echo l($menu_title, $menu_link);
        if (!empty($menu['below'])) :
          print off_canvas_responsive_menu_build_html($menu['below']);
        endif;
        ?>
      </li>
      <?php
    endif;
  endforeach;
  ?>
</ul>
