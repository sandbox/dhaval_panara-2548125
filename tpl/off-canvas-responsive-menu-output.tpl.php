<?php
/**
 * @file
 * Theme implementation to display a off canvas menu.
 *
 * Available variables:
 * - $menus: array of selected menus.
 */
global $base_url;
?>
<div class="off_canvas_responsive_menu_block">
  <div class="off_canvas_animate slide off_canvas_container close">
    <div class="off_canvas_animate slide off_canvas_top_menu">
      <div class="off_canvas_toggles">
        <span class="nav_prev_btn"><i class="icon-left"></i><?php print t('Back'); ?></span>
        <span class="nav_close_btn"><i class="icon-cancel"></i></span>
      </div>
    </div>
  </div>
  <div class="content_animate slide content">
    <header class="content_animate slide">
      <span class="nav_toggle">
        <i class="icon-menu"></i>
      </span>
      <nav class="dropdown">
        <ul>
          <?php
          foreach ($menus as $menu) :
            if (empty($menu['link']['hidden'])):
              $menu_title = $menu['link']['link_title'];
              $menu_link = (empty($menu['link']['link_path'])) ? $base_url : $menu['link']['link_path'];
              ?>
              <li>
                <?php
                print l($menu_title, $menu_link);
                if (!empty($menu['below'])):
                  $output = off_canvas_responsive_menu_build_html($menu['below']);
                  print $output;
                endif;
                ?>
              </li>
              <?php
            endif;
          endforeach;
          ?>
        </ul>
      </nav>
    </header>
  </div>
</div>
